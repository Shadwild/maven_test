package Generator;

import io.RegisterSerialiser;
import timer.WDTimer;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class AutoincrementID implements Serializable
{
    private volatile static AutoincrementID instance=null;
    private static int count;

    private AutoincrementID()
    {
        RegisterSerialiser r = new RegisterSerialiser("id.txt");
        count=0;
        int tst=0;
        try
        {
            tst=(int)r.read();
        }
        catch (Exception exception)
        {
            r.write(count);
            Logger.getAnonymousLogger().log(Level.WARNING, "Файл id.txt генератора индекса создан");
        }
    }
    public int generate()
    {
        RegisterSerialiser r = new RegisterSerialiser("id.txt");
        count++;
        r.write(count);
        return count;
    }
    public static AutoincrementID getInstance()
    {
        if (instance == null) {
            synchronized (AutoincrementID.class)
            {
                RegisterSerialiser r = new RegisterSerialiser("id.txt");
                if (instance == null)
                {
                    instance = new AutoincrementID();
                    count=(int)r.read();
                }
            }
        }
        return instance;
    }
}
