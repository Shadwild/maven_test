package Generator;

import java.time.LocalDateTime;

public class IDgenerator
{
    public static int generate()
    {
        return LocalDateTime.now().hashCode();
    };
}
