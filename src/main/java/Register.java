import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by VGolubeva on 20.11.2017.
 */
public class Register implements Serializable
{
    private ArrayList<TaskList> lists;

    public ArrayList<TaskList> getLists() {
        return lists;
    }

    public void setLists(ArrayList<TaskList> lists) {
        this.lists = lists;
    }
}
