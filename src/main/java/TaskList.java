import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by VGolubeva on 20.11.2017.
 */
public class TaskList implements Serializable
{
    private ArrayList<Task> tasks;
    private String name;

    public TaskList(String name) {
        this.tasks = new ArrayList<>();
        this.name = name;
    }

    public ArrayList<Task> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayList<Task> tasks) {
        this.tasks = tasks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void addTask(Task task)
    {
        tasks.add(task);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TaskList{");
        sb.append("tasks=").append(tasks);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
    public void sort()
    {
        Collections.sort(tasks, new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getDateTime().compareTo(o2.getDateTime());
            }
        });
    }
}
