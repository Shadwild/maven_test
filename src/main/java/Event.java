import Generator.IDgenerator;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Created by VGolubeva on 19.11.2017.
 */
public class Event implements Task
{
    private int ID;
    private String name;
    private String description;
    private LocalDateTime dateTime;
    private String contacts;
    private long secondsTo;

    public Event(String name, String description, LocalDateTime dateTime) {
        this.name = name;
        this.description = description;
        this.dateTime = dateTime;
        this.ID = IDgenerator.generate();
        secondsTo=recount(LocalDateTime.now());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public int getStatus() {
        return LocalDateTime.now().compareTo(dateTime);
    }

    public long getSecondsTo() {
        return secondsTo;
    }

    public void setSecondsTo(int secondsTo) {
        this.secondsTo = secondsTo;
    }

    public int getID() {
        return ID;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Event{");
        sb.append("name='")
                .append(name)
                .append('\'')
                .append(", description='")
                .append(description)
                .append('\'')
                .append(", dateTime=")
                .append(dateTime)
                .append(", contacts='")
                .append(contacts)
                .append('}');
        return sb.toString();
    }
    /*пересчет времени после последнего изменения/срабатывания
в качестве аргумента снятый момент времени
 */
    public long recount(LocalDateTime lastActionTime)
    {
        return dateTime.until(lastActionTime, ChronoUnit.SECONDS);
    }
}
