import java.io.Serializable;
import java.time.LocalDateTime;

public class AlarmEvent extends Event implements Serializable {
    private int repeatCount;
    private int repeatPeriod;

    public AlarmEvent(String name, String description, LocalDateTime dateTime, int repeatCount, int repeatPeriod) {
        super(name, description, dateTime);
        this.repeatCount = repeatCount;
        this.repeatPeriod = repeatPeriod;
    }
}
