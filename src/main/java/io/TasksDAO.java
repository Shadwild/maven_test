package io;

public interface TasksDAO
{
    void write(Object obj);
    Object read();
}
