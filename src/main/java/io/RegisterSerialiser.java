package io;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegisterSerialiser implements TasksDAO
{
    private final static String IO_FILE = "out.txt";
    private String ioFile = IO_FILE;
    private static Logger log = Logger.getAnonymousLogger();

    public RegisterSerialiser()
    {
    }

    public RegisterSerialiser(String ioFile)
    {
        this.ioFile=ioFile;
    }

    public String getIoFile() {
        return ioFile;
    }

    public void setIoFile(String ioFile) {
        this.ioFile = ioFile;
    }

    @Override
    public void write(Object obj)
    {
        try
        {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(ioFile));
            oos.writeObject(obj);
            oos.close();
        }
        catch(IOException e) {
            log.log(Level.WARNING, "Exception: ", e);
        }
    }

    @Override
    public Object read()
    {
        Object obj=null;
        try
        {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(ioFile));
            obj = ois.readObject();
            ois.close();
        }
        catch(IOException e)
        {
            log.log(Level.WARNING, "Exception: ", e);
        }
        catch(ClassNotFoundException e) {
            log.log(Level.WARNING, "Exception: ", e);
        }
        return obj;
    }
}
