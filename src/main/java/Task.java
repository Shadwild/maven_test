import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by VGolubeva on 19.11.2017.
 */
public interface Task extends Serializable {
    String getName();
    void setName(String name);
    String getDescription();
    void setDescription(String description);
    LocalDateTime getDateTime();
    void setDateTime(LocalDateTime dateTime);
    String getContacts();
    void setContacts(String contacts);
    int getStatus();
    public long getSecondsTo();
    public void setSecondsTo(int secondsTo);
    String toString();
}
