import Generator.AutoincrementID;
import Generator.IDgenerator;
import io.RegisterSerialiser;
import timer.WDTimer;

import java.time.LocalDateTime;

public class TaskManager
{
    public static void main(String args[])
    {
       /* //пример вызова таймера и установки задачи
        WDTimer timer = WDTimer.getInstance(); //установка задачи которая сработает через 5 секунд
        timer.setWaitTime(5);
        timer.setTask(() -> System.out.println("Хоп, хей, ла ла лей"));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        timer.cancelTack(); //однако основной поток проснется через 2 секунды и отменяет ее.
        timer.setWaitTime(10);//время до новой задачи
        timer.setTask(() -> System.out.println("Хоп, хей, ла ла лей2"));//действие при наступлении времени задачи
        timer.destroyTimer();
        //конец примера*/
        AutoincrementID id = AutoincrementID.getInstance();
        for (int i=0;i<20;i++)
        System.out.println(id.generate());
    }
}
